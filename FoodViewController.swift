//
//  FoodViewController.swift
//  Good Papaya
//
//  Created by Geddy on 24/03/2017.
//  Copyright © 2017 Smilez. All rights reserved.
//

import UIKit


class FoodViewController: UIViewController  {
    
    @IBOutlet weak var foodView: UICollectionView!
    
    var imageName = "spaghettisx"
    var food = [FoodCollectionViewCell]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        food.append(FoodCollectionViewCell(imageName: "spaghettisx", label: "We Bare Bears Calzones"))
        
        // Do any additional setup after loading the view.
    }
    
    
    
}
private struct Storyboard {
    static let CellIdentifier = "Food Cell"
}

extension FoodViewController : UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return food.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = foodView.dequeueReusableCell(withReuseIdentifier: Storyboard.CellIdentifier, for: indexPath)
        collectionView.register(FoodCollectionViewCell.self, forCellWithReuseIdentifier: "Food Cell")
        return cell
    }
}
